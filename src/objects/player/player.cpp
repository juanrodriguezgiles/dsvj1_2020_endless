#include "player.h"
using namespace endless;
using namespace game;
using namespace obstacle;
namespace endless
{
	namespace player
	{
		playerObject player1;

		static void jump()
		{
			static const int jumpHeight = 150;
			static int startPosition;
			static int endPosition;
			static int aux = 0;
			switch (player1.lane)
			{
			case 1:
				startPosition = endless::screen::lane1;
				endPosition = endless::screen::lane1 - jumpHeight;
				break;
			case 2:
				startPosition = endless::screen::lane2;
				endPosition = endless::screen::lane2 - jumpHeight;
				break;
			case 3:
				startPosition = endless::screen::lane3;
				endPosition = endless::screen::lane3 - jumpHeight;
				break;
			}
			if (player1.tagJumping)
			{
				if (aux < 2)
				{
					UpdateMusicStream(player1.soundJump);
					aux++;
				}
				if (!player1.tagJumpingUp && !player1.tagJumpingDown)
				{
					player1.tagJumpingUp = true;
				}
				if (player1.tagJumpingUp)
				{
					if (player1.body.y <= endPosition)
					{
						player1.tagJumpingUp = false;
						player1.tagJumpingDown = true;
					}
					else
					{
						player1.body.y -= player1.speed.y * GetFrameTime();
					}
				}
				if (player1.tagJumpingDown)
				{
					if (player1.body.y >= startPosition)
					{
						player1.tagJumpingDown = false;
						player1.tagJumping = false;
						aux = 0;
					}
					else
					{
						player1.body.y += player1.speed.y * GetFrameTime();
					}
				}
			}
		}
		static void changeTexture()
		{
			static int aux = 0;
			if (player1.tagAttack)
			{
				player1.textureCurrent = player1.textureAttack;
				aux++;
				if (aux > 10)
				{
					player1.textureCurrent = player1.textureNormal;
					player1.tagAttack = false;
					aux = 0;
				}
			}
		}
		static void checkGameOver()
		{
			if (!player1.tagAlive)
			{
				UpdateMusicStream(player1.soundDeath);
				currentScene = scene::game_over;
				game::init();
			}
		}
		static void updateScore()
		{
			player1.score += 1;
		}
		void initPlayer()
		{
			player1.body.x = endless::screen::screenWidth / 20;
			player1.body.y = endless::screen::screenMiddleY;
			player1.body.width = 80;
			player1.body.height = 80;
			player1.speed.x = 200.0f;
			player1.speed.y = 200.0f;
			player1.textureNormal = LoadTexture("res/assets/textures/Ducky.png"); //tgfcoder
			player1.textureAttack = LoadTexture("res/assets/textures/DuckyFire.png"); //tgfcoder
			player1.textureCurrent = player1.textureNormal;
			player1.soundJump = LoadMusicStream("res/assets/sounds/jump.ogg"); //Blender Foundation
			player1.soundAttack = LoadMusicStream("res/assets/sounds/attack.ogg"); //dklon
			player1.soundDash = LoadMusicStream("res/assets/sounds/dash.ogg"); //Blender Foundation
			player1.soundDeath = LoadMusicStream("res/assets/sounds/death.ogg"); //sauer2
			player1.color = WHITE;
			player1.score = 0;
			player1.lane = 2;
			player1.tagAlive = true;
			player1.tagJumping = false;
			player1.tagJumpingUp = false;
			player1.tagJumpingDown = false;
			player1.tagAttack = false;
			PlayMusicStream(player1.soundJump);
			PlayMusicStream(player1.soundAttack);
			PlayMusicStream(player1.soundDash);
			PlayMusicStream(player1.soundDeath);
			SetMusicVolume(player1.soundAttack, 0.09f);
		}
		void deInitPlayer()
		{
			UnloadTexture(player1.textureNormal);
			UnloadTexture(player1.textureAttack);
			UnloadTexture(player1.textureCurrent);
			UnloadMusicStream(player1.soundJump);
			UnloadMusicStream(player1.soundAttack);
			UnloadMusicStream(player1.soundDash);
			UnloadMusicStream(player1.soundDeath);
		}
		void drawPlayer()
		{
#if DEBUG
			DrawRectangleLines(player1.body.x, player1.body.y, player1.body.width, player1.body.height, player1.color);
#endif
			DrawTexture(player1.textureCurrent, player1.body.x, player1.body.y, player1.color);
		}
		void moveUp()
		{
			static int aux = 0;
			if (!player1.tagJumping)
			{
				if (player1.lane == 2)
				{
					UpdateMusicStream(player1.soundDash);
					player1.lane--;
					player1.body.y = endless::screen::lane1;
				}
				else if (player1.lane == 3)
				{
					UpdateMusicStream(player1.soundDash);
					player1.lane--;
					player1.body.y = endless::screen::lane2;
				}
			}
		}
		void moveDown()
		{
			if (!player1.tagJumping)
			{
				if (player1.lane == 1)
				{
					UpdateMusicStream(player1.soundDash);
					player1.lane++;
					player1.body.y = endless::screen::lane2;
				}
				else if (player1.lane == 2)
				{
					UpdateMusicStream(player1.soundDash);
					player1.lane++;
					player1.body.y = endless::screen::lane3;
				}
			}
		}
		void attack()
		{
			if (!player1.tagJumping)
			{
				for (int i = 0; i < obstaclesPerLane; i++)
				{
					if (obstacles[player1.lane - 1][i].active && obstacles[player1.lane - 1][i].destructible &&
						obstacles[player1.lane - 1][i].body.x < player1.body.x + player1.body.width + 50)
					{
						UpdateMusicStream(player1.soundAttack);
						UpdateMusicStream(player1.soundAttack);
						obstacles[player1.lane - 1][i].active = false;
						player1.tagAttack = true;
						player1.score += 50;
					}
				}
			}
		}
		void updatePlayer()
		{
			checkGameOver();
			if (player1.tagJumping)
				jump();
			changeTexture();
			updateScore();
		}
	}
}
