#ifndef PLAYER_H
#define PLAYER_H
#include "raylib.h"
#include "game/game.h"
#include "game/screen/screen.h"
#include "objects/obstacles/obstacle.h"
namespace endless
{
	namespace player
	{	
		struct playerObject
		{
			Rectangle body;
			Vector2 speed;
			Texture2D textureCurrent;
			Texture2D textureNormal;
			Texture2D textureAttack;
			Music soundJump; 
			Music soundAttack;
			Music soundDash;
			Music soundDeath;
			Color color;
			int score;
			int lane;
			bool tagAlive;
			bool tagJumping;
			bool tagJumpingUp;
			bool tagJumpingDown;
			bool tagAttack;
		};
		extern playerObject player1;

		void initPlayer();
		void deInitPlayer();
		void drawPlayer();
		void jump();
		void moveUp();
		void moveDown();
		void attack();
		void changeTexture();
		void updatePlayer();
	}
}
#endif
