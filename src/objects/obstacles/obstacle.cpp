#include <iostream>
#include <random>
#include "obstacle.h"
using namespace endless;
using namespace screen;
using namespace player;
namespace endless
{
	namespace obstacle
	{
		static const int lanes = 3;
		const int obstaclesPerLane = 10;
		obstacleObject obstacles[lanes][obstaclesPerLane];
		static Texture2D dodgeable;
		static Texture2D undodgeable;
		static Texture2D destructible;

		static void resetObstacles()
		{
			static int aux;

			for (short i = 0; i < lanes; i++)
			{
				for (int j = 0; j < obstaclesPerLane; j++)
				{
					if (obstacles[i][j].body.x <= 0)
					{
						aux = rand() % 4;
						switch (aux)
						{
						case 0: //dodgeable
							obstacles[i][j].body.width = 40;
							obstacles[i][j].body.height = 20;
							obstacles[i][j].body.x = screenWidth - obstacles[i][j].body.x;
							switch (i)
							{
							case 0:
								obstacles[i][j].body.y = lane1 + obstacles[i][j].body.height * 3;
								break;
							case 1:
								obstacles[i][j].body.y = lane2 + obstacles[i][j].body.height * 3;
								break;
							case 2:
								obstacles[i][j].body.y = lane3 + obstacles[i][j].body.height * 3;
								break;
							}
							obstacles[i][j].texture = dodgeable;
							obstacles[i][j].color = WHITE;
							obstacles[i][j].active = true;
							obstacles[i][j].destructible = false;
							obstacles[i][j].dodgeable = true;
							break;
						case 1: //undodgeable
							obstacles[i][j].body.width = 40;
							obstacles[i][j].body.height = 80;
							obstacles[i][j].body.x = screenWidth - obstacles[i][j].body.x;
							switch (i)
							{
							case 0:
								obstacles[i][j].body.y = lane1;
								break;
							case 1:
								obstacles[i][j].body.y = lane2;
								break;
							case 2:
								obstacles[i][j].body.y = lane3;
								break;
							}
							obstacles[i][j].texture = undodgeable;
							obstacles[i][j].color = WHITE;
							obstacles[i][j].active = true;
							obstacles[i][j].destructible = false;
							obstacles[i][j].dodgeable = false;
							break;
						case 2: //destructible
							obstacles[i][j].body.width = 40;
							obstacles[i][j].body.height = 40;
							obstacles[i][j].body.x = screenWidth - obstacles[i][j].body.x;
							switch (i)
							{
							case 0:
								obstacles[i][j].body.y = lane1 + obstacles[i][j].body.height;
								break;
							case 1:
								obstacles[i][j].body.y = lane2 + obstacles[i][j].body.height;
								break;
							case 2:
								obstacles[i][j].body.y = lane3 + obstacles[i][j].body.height;
								break;
							}
							obstacles[i][j].texture = destructible;
							obstacles[i][j].color = WHITE;
							obstacles[i][j].active = true;
							obstacles[i][j].destructible = true;
							obstacles[i][j].dodgeable = false;
							break;
						default:
							obstacles[i][j].body.x = screenWidth - obstacles[i][j].body.x;
							switch (i)
							{
							case 0:
								obstacles[i][j].body.y = lane1;
								break;
							case 1:
								obstacles[i][j].body.y = lane2;
								break;
							case 2:
								obstacles[i][j].body.y = lane3;
								break;
							}
							obstacles[i][j].active = false;
							break;
						}
					}
				}
			}

		}
		static void moveObstacles()
		{
			for (int i = 0; i < lanes; i++)
			{
				for (int j = 0; j < obstaclesPerLane; j++)
				{
					obstacles[i][j].body.x -= obstacles[i][j].speed * GetFrameTime();
				}
			}
		}
		static void checkCollisionObstacles()
		{
			for (int i = 0; i < obstaclesPerLane; i++)
			{
				if (obstacles[player1.lane - 1][i].active && CheckCollisionRecs(player1.body, obstacles[player1.lane - 1][i].body))
				{
					player1.tagAlive = false;
				}
			}
		}
		void updateObstacles()
		{
			resetObstacles();
			checkCollisionObstacles();
			moveObstacles();
		}
		void initObstacles()
		{
			static int aux;
			dodgeable = LoadTexture("res/assets/textures/poop.png");
			undodgeable = LoadTexture("res/assets/textures/barrell.png");
			destructible = LoadTexture("res/assets/textures/enemy.png");
			for (int i = 0; i < lanes; i++)
			{
				for (int j = 0; j < obstaclesPerLane; j++)
				{
					aux = rand() % 4;
					switch (aux)
					{
					case 0: //dodgeable
						obstacles[i][j].body.width = 40;
						obstacles[i][j].body.height = 20;
						obstacles[i][j].body.x = screenWidth + j * obstacleSpacing * 3;
						switch (i)
						{
						case 0:
							obstacles[i][j].body.y = lane1 + obstacles[i][j].body.height * 3;
							break;
						case 1:
							obstacles[i][j].body.y = lane2 + obstacles[i][j].body.height * 3;
							break;
						case 2:
							obstacles[i][j].body.y = lane3 + obstacles[i][j].body.height * 3;
							break;
						}
						obstacles[i][j].speed = 125.0f;
						obstacles[i][j].texture = dodgeable;
						obstacles[i][j].color = WHITE;
						obstacles[i][j].active = true;
						obstacles[i][j].destructible = false;
						obstacles[i][j].dodgeable = true;
						break;
					case 1: //undodgeable
						obstacles[i][j].body.width = 40;
						obstacles[i][j].body.height = 80;
						obstacles[i][j].body.x = screenWidth + j * obstacleSpacing * 3;
						switch (i)
						{
						case 0:
							obstacles[i][j].body.y = lane1;
							break;
						case 1:
							obstacles[i][j].body.y = lane2;
							break;
						case 2:
							obstacles[i][j].body.y = lane3;
							break;
						}
						obstacles[i][j].speed = 125.0f;
						obstacles[i][j].texture = undodgeable;
						obstacles[i][j].color = WHITE;
						obstacles[i][j].active = true;
						obstacles[i][j].destructible = false;
						obstacles[i][j].dodgeable = false;
						break;
					case 2: //destructible
						obstacles[i][j].body.width = 40;
						obstacles[i][j].body.height = 40;
						obstacles[i][j].body.x = screenWidth + j * obstacleSpacing * 3;
						switch (i)
						{
						case 0:
							obstacles[i][j].body.y = lane1 + obstacles[i][j].body.height;
							break;
						case 1:
							obstacles[i][j].body.y = lane2 + obstacles[i][j].body.height;
							break;
						case 2:
							obstacles[i][j].body.y = lane3 + obstacles[i][j].body.height;
							break;
						}
						obstacles[i][j].speed = 125.0f;
						obstacles[i][j].texture = destructible;
						obstacles[i][j].color = WHITE;
						obstacles[i][j].active = true;
						obstacles[i][j].destructible = true;
						obstacles[i][j].dodgeable = false;
						break;
					default:
						obstacles[i][j].body.x = screenWidth + j * obstacleSpacing * 3;
						switch (i)
						{
						case 0:
							obstacles[i][j].body.y = lane1;
							break;
						case 1:
							obstacles[i][j].body.y = lane2;
							break;
						case 2:
							obstacles[i][j].body.y = lane3;
							break;
						}
						obstacles[i][j].speed = 125.0f;
						obstacles[i][j].active = false;
						break;
					}
				}
			}
		}
		void deInitObstacles()
		{
			UnloadTexture(undodgeable);
			UnloadTexture(dodgeable);
			UnloadTexture(destructible);
		}
		void drawObstacles()
		{
			for (int i = 0; i < lanes; i++)
			{
				for (int j = 0; j < obstaclesPerLane; j++)
				{
					if (obstacles[i][j].active)
					{
#if DEBUG
						DrawRectangleLinesEx(obstacles[i][j].body, 2, WHITE);
#endif
						if (obstacles[i][j].destructible)
							DrawTexture(obstacles[i][j].texture, obstacles[i][j].body.x, obstacles[i][j].body.y - 20, obstacles[i][j].color);
						else
							DrawTexture(obstacles[i][j].texture, obstacles[i][j].body.x, obstacles[i][j].body.y, obstacles[i][j].color);
					}
				}
			}
		}
	}
}