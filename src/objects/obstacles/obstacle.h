#ifndef OBSTACLE_H
#define OBSTACLE_H
#include "raylib.h"
#include "game/screen/screen.h"
#include "objects/player/player.h"
namespace endless
{
	namespace obstacle
	{
		struct obstacleObject
		{
			Rectangle body;
			float speed;
			Color color;
			bool active;
			bool dodgeable;
			bool destructible;
			Texture2D texture;
			Music sound;
		};
		extern const int obstaclesPerLane;
		extern obstacleObject obstacles[3][10];

		void updateObstacles();
		void initObstacles();
		void deInitObstacles();
		void drawObstacles();
	}
}
#endif