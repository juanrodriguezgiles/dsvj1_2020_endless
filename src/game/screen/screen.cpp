#include "screen.h"
namespace endless
{
	namespace screen
	{
		const int screenWidth = 1600;
		const int screenHeight = 800;
		const int screenMiddleX = screenWidth / 2;
		const int screenMiddleY = screenHeight / 2;
		const int textSpacing = 100;
		const int obstacleSpacing = 100;
		const int lane1 = obstacleSpacing + obstacleSpacing / 2;
		const int lane2 = screenMiddleY;
		const int lane3 = screenHeight - obstacleSpacing;
		Texture2D background;
		static Texture2D textureLane1;
		static Texture2D back;
		static Texture2D mid;
		static Texture2D front;
		float scrollingBack = 0.0f;
		float scrollingMid = 0.0f;
		float scrollingFore = 0.0f;

		void drawScreen()
		{
			DrawTextureEx(back, (Vector2{ scrollingBack, 100 }), 0.0f, 2.0f, WHITE);
			DrawTextureEx(back, (Vector2{ back.width * 2 + scrollingBack, 100 }), 0.0f, 2.0f, WHITE);
			DrawTextureEx(mid, (Vector2{ scrollingMid, 100 }), 0.0f, 2.0f, WHITE);
			DrawTextureEx(mid, (Vector2{ mid.width * 2 + scrollingMid, 100 }), 0.0f, 2.0f, WHITE);
			DrawTextureEx(front, (Vector2{ scrollingFore, 179 }), 0.0f, 2.0f, WHITE);
			DrawTextureEx(front, (Vector2{ front.width * 2 + scrollingFore, 179 }), 0.0f, 2.0f, WHITE);
			DrawTexture(textureLane1, 0, lane1 + 80, WHITE);
			DrawTexture(textureLane1, 0, lane2 + 80, WHITE);
			DrawTexture(textureLane1, 0, lane3 + 80, WHITE);
		}
		void initScreen()
		{
			back = LoadTexture("res/assets/textures/back.png");
			mid = LoadTexture("res/assets/textures/mid.png");
			front = LoadTexture("res/assets/textures/front.png");
			background = LoadTexture("res/assets/textures/background1.png");
			textureLane1 = LoadTexture("res/assets/textures/lane1.png");
		}
		void deInitScreen()
		{
			UnloadTexture(back);
			UnloadTexture(mid);
			UnloadTexture(front);
			UnloadTexture(background);
			UnloadTexture(textureLane1);
		}
		void updateScreen()
		{
			scrollingBack -= 0.1f;
			scrollingMid -= 0.5f;
			scrollingFore -= 1.0f;

			if (scrollingBack <= -back.width * 2) scrollingBack = 0;
			if (scrollingMid <= -mid.width * 2) scrollingMid = 0;
			if (scrollingFore <= -front.width * 2) scrollingFore = 0;
		}
	}
}