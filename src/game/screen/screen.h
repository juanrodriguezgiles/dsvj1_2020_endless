#ifndef SCREEN_H
#define SCREEN_H
#include "raylib.h"
namespace endless
{
	namespace screen
	{
		extern const int screenWidth;
		extern const int screenHeight;
		extern const int screenMiddleX;
		extern const int screenMiddleY;
		extern const int textSpacing;
		extern const int obstacleSpacing;
		extern const int lane1;
		extern const int lane2;
		extern const int lane3;
		extern Texture2D background;
		extern float scrollingBack;
		extern float scrollingMid;
		extern float scrollingFore;
		void drawScreen();
		void initScreen();
		void deInitScreen();
		void updateScreen();
	}
}
#endif