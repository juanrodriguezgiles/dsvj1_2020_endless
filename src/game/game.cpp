#include "game.h"
#include "raylib.h"
namespace endless
{
	namespace game
	{
		scene currentScene = scene::main_menu;
		bool playing = true;

		void init()
		{
			switch (currentScene)
			{
			case endless::game::scene::main_menu:
				endless::main_menu::init();
				break;
			case endless::game::scene::credits:
				endless::credits::init();
				break;
			case endless::game::scene::gameplay:
				endless::gameplay::init();
				break;
			case endless::game::scene::pause:
				endless::pause::init();
				break;
			case endless::game::scene::exitGame:
				endless::exitGame::init();
				break;
			case endless::game::scene::game_over:
				endless::game_over::init();
				break;
			default:
				break;
			}
		}
		static void input()
		{
			switch (currentScene)
			{
			case endless::game::scene::main_menu:
				endless::main_menu::input();
				break;
			case endless::game::scene::credits:
				endless::credits::input();
				break;
			case endless::game::scene::gameplay:
				endless::gameplay::input();
				break;
			case endless::game::scene::pause:
				endless::pause::input();
				break;
			case endless::game::scene::exitGame:
				endless::exitGame::input();
				break;
			case endless::game::scene::game_over:
				endless::game_over::input();
				break;
			default:
				break;
			}
		}
		static void update()
		{
			switch (currentScene)
			{
			case endless::game::scene::main_menu:
				endless::main_menu::update();
				break;
			case endless::game::scene::credits:
				endless::credits::update();
				break;
			case endless::game::scene::gameplay:
				endless::gameplay::update();
				break;
			case endless::game::scene::pause:
				endless::pause::update();
				break;
			case endless::game::scene::exitGame:
				endless::exitGame::update();
				break;
			case endless::game::scene::game_over:
				endless::game_over::update();
				break;
			default:
				break;
			}
		}
		static void draw()
		{
			switch (currentScene)
			{
			case endless::game::scene::main_menu:
				endless::main_menu::draw();
				break;
			case endless::game::scene::credits:
				endless::credits::draw();
				break;
			case endless::game::scene::gameplay:
				endless::gameplay::draw();
				break;
			case endless::game::scene::pause:
				endless::pause::draw();
				break;
			case endless::game::scene::exitGame:
				endless::exitGame::draw();
				break;
			case endless::game::scene::game_over:
				endless::game_over::draw();
				break;
			default:
				break;
			}
		}
		void deInit()
		{
			CloseAudioDevice();
			CloseWindow();
			screen::deInitScreen();
			obstacle::deInitObstacles();
			player::deInitPlayer();
		}
		void run()
		{
			while (!WindowShouldClose() && playing)
			{
				input();
				update();
				draw();
			}
		}
	}
}