#ifndef PAUSE_H
#define PAUSE_H
#include "raylib.h"
#include "game/game.h"
#include "game/screen/screen.h"
#include "scenes/gameplay/gameplay.h"
#include "scenes/main_menu/main_menu.h"
namespace endless
{
	namespace pause
	{
		void init();
		void input();
		void update();
		void draw();
		void deInit();
	}
}
#endif