#include "pause.h"
using namespace endless;
using namespace game;
using namespace screen;
using namespace gameplay;
using namespace main_menu;
namespace endless
{
	namespace pause
	{
		static Music pauseSelect;

		void init()
		{
			pauseSelect = LoadMusicStream("res/assets/sounds/menuSelect.ogg"); //NenadSimic
			PlayMusicStream(pauseSelect);
		}
		void input()
		{
			if (IsKeyPressed(KEY_ONE))
			{
				UpdateMusicStream(pauseSelect);
				currentScene = scene::gameplay;
			}
			if (IsKeyPressed(KEY_TWO))
			{
				UpdateMusicStream(pauseSelect);
				currentScene = scene::main_menu;
				game::init();
			}
		}
		void update()
		{
			UpdateMusicStream(musicGameplay);
		}
		void draw()
		{
			BeginDrawing();
			ClearBackground(BLACK);
			DrawTexture(background, 0, 0, WHITE);
			DrawTexture(uiNormal, screenMiddleX - 190, screenMiddleY / 2, WHITE);
			DrawText("PAUSE", (screenMiddleX - (MeasureText("PAUSE", 50) / 2)), screenMiddleY / 2, 50, WHITE);
			DrawTexture(uiNormal, screenMiddleX - 190, screenMiddleY, WHITE);
			DrawText("1. RESUME", (screenMiddleX - (MeasureText("1. RESUME", 50) / 2)), screenMiddleY, 50, WHITE);
			DrawTexture(uiNormal, screenMiddleX - 190, screenMiddleY + 100, WHITE);
			DrawText("2. MAIN MENU", (screenMiddleX - (MeasureText("2. MAIN MENU", 50) / 2)), screenMiddleY + textSpacing, 50, WHITE);
			EndDrawing();
		}
		void deInit()
		{

		}
	}
}