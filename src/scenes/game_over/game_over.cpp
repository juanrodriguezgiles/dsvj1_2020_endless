#include "game_over.h"
using namespace endless;
using namespace game;
using namespace screen;
using namespace gameplay;
using namespace main_menu;
namespace endless
{
	namespace game_over
	{
		static Music gameOverSelect;
		void init()
		{
			gameOverSelect = LoadMusicStream("res/assets/sounds/menuSelect.ogg"); //NenadSimic
			PlayMusicStream(gameOverSelect);
		}
		void input()
		{
			if (IsKeyPressed(KEY_ONE))
			{
				UpdateMusicStream(gameOverSelect);
				currentScene = scene::gameplay;
				game::init();
			}
			if (IsKeyPressed(KEY_TWO))
			{
				UpdateMusicStream(gameOverSelect);
				currentScene = scene::main_menu;
				game::init();
			}
		}
		void update()
		{
			UpdateMusicStream(musicGameplay);
		}
		void draw()
		{
			BeginDrawing();
			ClearBackground(BLACK);
			DrawTexture(background, 0, 0, WHITE);
			DrawTexture(uiNormal, screenMiddleX - 190, textSpacing * 2, WHITE);
			DrawTexture(uiNormal, screenMiddleX - 190, textSpacing * 4, WHITE);
			DrawTexture(uiNormal, screenMiddleX - 190, textSpacing * 5, WHITE);
			DrawText("GAME OVER", (screenMiddleX - (MeasureText("GAME OVER", 50) / 2)), screenMiddleY / 2, 50, WHITE);
			DrawText("1. PLAY", (screenMiddleX - (MeasureText("1. PLAY", 50) / 2)), screenMiddleY, 50, WHITE);
			DrawText("2. MAIN MENU", (screenMiddleX - (MeasureText("2. MAIN MENU", 50) / 2)), screenMiddleY + 100, 50, WHITE);
			EndDrawing();
		}
		void deInit()
		{

		}
	}
}