#ifndef EXIT_H
#define EXIT_H
#include "raylib.h"
#include "game/game.h"
#include "game/screen/screen.h"
#include "scenes/main_menu/main_menu.h"
namespace endless
{
	namespace exitGame
	{
		void init();
		void input();
		void update();
		void draw();
		void deInit();
	}
}
#endif