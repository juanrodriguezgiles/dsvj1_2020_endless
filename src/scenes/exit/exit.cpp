#include "exit.h"
using namespace endless;
using namespace game;
using namespace screen;
using namespace main_menu;
namespace endless
{
	namespace exitGame
	{
		static Music exitSelect;

		void init()
		{
			exitSelect = LoadMusicStream("res/assets/sounds/menuSelect.ogg"); //NenadSimic
			PlayMusicStream(exitSelect);
		}
		void input()
		{
			if (IsKeyPressed(KEY_ONE))
			{
				UpdateMusicStream(exitSelect);
				currentScene = scene::main_menu;
				game::init();
			}
			if (IsKeyPressed(KEY_TWO))
			{
				UpdateMusicStream(exitSelect);
				game::deInit();
				playing = false;
			}
		}
		void update()
		{
			UpdateMusicStream(mainMenuMusic);
		}
		void draw()
		{
			BeginDrawing();
			ClearBackground(BLACK);
			DrawTexture(background, 0, 0, WHITE);
			DrawTexture(uiNormal, screenMiddleX - 190, screenMiddleY / 2, WHITE);
			DrawText("EXIT?", (screenMiddleX - (MeasureText("EXIT?", 50) / 2)), screenMiddleY / 2, 50, WHITE);
			DrawTexture(uiNormal, screenMiddleX - 190, screenMiddleY, WHITE);
			DrawText("1. NO", (screenMiddleX - (MeasureText("1. NO", 50) / 2)), screenMiddleY, 50, WHITE);
			DrawTexture(uiNormal, screenMiddleX - 190, screenMiddleY + 100, WHITE);
			DrawText("2. YES", (screenMiddleX - (MeasureText("2. YES", 50) / 2)), screenMiddleY + textSpacing, 50, WHITE);
			EndDrawing();
		}
		void deInit()
		{

		}
	}
}