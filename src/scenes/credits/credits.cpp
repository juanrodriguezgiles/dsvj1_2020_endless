#include "credits.h"
using namespace endless;
using namespace game;
using namespace screen;
using namespace main_menu;
namespace endless
{
	namespace credits
	{
		static Music creditsSelect;

		void init()
		{
			creditsSelect = LoadMusicStream("res/assets/sounds/menuSelect.ogg"); //NenadSimic
			PlayMusicStream(creditsSelect);
		}
		void input()
		{
			if (IsKeyPressed(KEY_ONE))
			{
				UpdateMusicStream(creditsSelect);
				currentScene = scene::main_menu;
				game::init();
			}
		}
		void update()
		{
			UpdateMusicStream(mainMenuMusic);
		}
		void draw()
		{
			BeginDrawing();
			ClearBackground(BLACK);
			DrawTexture(background, 0, 0, WHITE);
			DrawText("CREDITS", (screenMiddleX - (MeasureText("CREDITS", 50) / 2)), screenMiddleY / 2, 50, WHITE);
			DrawText("SOUND AND ART: NenadSimic, TinyWorlds, tgfcoder", (screenMiddleX - (MeasureText("SOUND AND ART: NenadSimic, TinyWorlds, tgfcoder", 22) / 2)), screenMiddleY, 22, WHITE);
			DrawText("Blender Foundation, dklon, sauer2, bevouliin.com, MystiqMiu", (screenMiddleX - (MeasureText("Blender Foundation, dklon, sauer2, bevouliin.com, MystiqMiu", 22) / 2)), screenMiddleY + textSpacing / 2, 22, WHITE);
			DrawText("DESIGNED AND CODED BY JUAN RODRIGUEZ GILES", (screenMiddleX - (MeasureText("DESIGNED AND CODED BY JUAN RODRIGUEZ GILES", 22) / 2)), screenMiddleY + textSpacing * 2, 22, WHITE);
			DrawTexture(uiNormal, screenMiddleX - 190, screenHeight - 50, WHITE);
			DrawText("1. MAIN MENU", (screenMiddleX - (MeasureText("1. MAIN MENU", 30) / 2)), screenHeight - 40, 30, WHITE);
			EndDrawing();
		}
		void deInit()
		{

		}
	}
}