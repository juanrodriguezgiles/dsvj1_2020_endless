#ifndef GAMEPLAY_H
#define GAMEPLAY_H
#include "raylib.h"
#include "game/game.h"
#include "game/screen/screen.h"
#include "objects/player/player.h"
#include "objects/obstacles/obstacle.h"
namespace endless
{
	namespace gameplay
	{
		extern Music musicGameplay;

		void init();
		void input();
		void update();
		void draw();
		void deInit();
	}
}
#endif