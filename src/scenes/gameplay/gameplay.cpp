#include "gameplay.h"
using namespace endless;
using namespace game;
using namespace screen;
using namespace player;
using namespace obstacle;
namespace endless
{
	namespace gameplay
	{
		Music musicGameplay;

		static void initMusic()
		{
			musicGameplay = LoadMusicStream("res/assets/sounds/musicGameplay1.ogg");
			PlayMusicStream(musicGameplay);
			SetMusicVolume(musicGameplay, 0.09f);
		}
		void init()
		{
			initPlayer();
			initObstacles();
			initMusic();
		}
		void input()
		{
			if (IsKeyPressed(KEY_SPACE))
			{
				player1.tagJumping = true;
			}
			if (IsKeyPressed(KEY_A))
			{
				attack();
			}
			if (IsKeyPressed(KEY_UP))
			{
				moveUp();
			}
			if (IsKeyPressed(KEY_DOWN))
			{
				moveDown();
			}
			if (IsKeyPressed(KEY_P))
			{
				currentScene = scene::pause;
				game::init();
			}
		}
		void update()
		{
			updatePlayer();
			updateObstacles();
			UpdateMusicStream(musicGameplay);
			updateScreen();
		}
		void draw()
		{
			BeginDrawing();
			ClearBackground(BLACK);
			drawScreen();
			DrawText("SCORE:", screenMiddleX - (MeasureText("SCORE", 25)), obstacleSpacing / 4, 25, RED);
			DrawText(FormatText("%05i", player1.score), screenMiddleX + 10, obstacleSpacing / 4, 25, RED);
			drawPlayer();
			drawObstacles();
			EndDrawing();
		}
		void deInit()
		{

		}
	}
}