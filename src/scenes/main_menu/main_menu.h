#ifndef MAIN_MENU_H
#define MAIN_MENU_H
#include "raylib.h"
#include "game/game.h"
#include "game/screen/screen.h"
namespace endless
{
	namespace main_menu
	{
		extern Music mainMenuMusic;
		extern Texture2D uiNormal;

		void init();
		void input();
		void update();
		void draw();
		void deInit();
	}
}
#endif
