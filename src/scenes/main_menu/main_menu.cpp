#include <random>
#include <ctime>
#include "main_menu.h"
using namespace endless;
using namespace game;
using namespace screen;
namespace endless
{
	namespace main_menu
	{
		static bool firstRun = true;
		Music mainMenuMusic;
		static Music menuSelect;
		Texture2D uiNormal;

		void init()
		{
			if (firstRun)
			{
				InitWindow(screenWidth, screenHeight, "ENDLESS");
				InitAudioDevice();
				SetTargetFPS(60);
				srand(time(NULL));
				firstRun = false;
				mainMenuMusic = LoadMusicStream("res/assets/sounds/mainMenu.ogg"); //TinyWorlds
				PlayMusicStream(mainMenuMusic);
				initScreen();
				uiNormal = LoadTexture("res/assets/textures/uiN1.png");
			}
			menuSelect = LoadMusicStream("res/assets/sounds/menuSelect.ogg"); //NenadSimic
			PlayMusicStream(menuSelect);
		}
		void input()
		{
			if (IsKeyPressed(KEY_ONE))
			{
				UpdateMusicStream(menuSelect);
				currentScene = scene::gameplay;
				game::init();
			}
			if (IsKeyPressed(KEY_TWO))
			{
				UpdateMusicStream(menuSelect);
				currentScene = scene::credits;
				game::init();
			}
			if (IsKeyPressed(KEY_THREE))
			{
				currentScene = scene::exitGame;
				game::init();
			}
		}
		void update()
		{
			UpdateMusicStream(mainMenuMusic);
		}
		void draw()
		{
			BeginDrawing();
			ClearBackground(BLACK);
			DrawTexture(background, 0, 0, WHITE);
			DrawText("ENDLESS DUCKY", (screenMiddleX - (MeasureText("ENDLESS DUCKY", 50) / 2)), textSpacing, 50, RED);
			DrawTexture(uiNormal, screenMiddleX - 190, textSpacing * 3, WHITE);
			DrawText("1. PLAY", (screenMiddleX - (MeasureText("1. PLAY", 50) / 2)), textSpacing * 3, 50, WHITE);
			DrawTexture(uiNormal, screenMiddleX - 190, textSpacing * 4, WHITE);
			DrawText("2. CREDITS", (screenMiddleX - (MeasureText("2. CREDITS", 50) / 2)), textSpacing * 4, 50, WHITE);
			DrawTexture(uiNormal, screenMiddleX - 190, textSpacing * 5, WHITE);
			DrawText("3. EXIT", (screenMiddleX - (MeasureText("3. EXIT", 50) / 2)), textSpacing * 5, 50, WHITE);
			DrawText("v0.2", 0, screenHeight - 25, 30, WHITE);
			EndDrawing();
		}
		void deInit()
		{
			
		}
	}
}